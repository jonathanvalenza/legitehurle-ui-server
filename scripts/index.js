//création de la classe administrateur
var administrateur = {
    idadministrateur : "",
    nom : "",
    email : "",
    password : "",
};
//création de la classe information
var information = {
    idinformation : "",
    email_client : "",
    message : "",
};
//création de la classe reservation
var reservation = {
    idreservation : "",
    date_reservation : "",
    email : "",
    debut_sejour : "",
    fin_sejour : "",
    id_espace : "",
    id_formule : "",
};
//création de la classe formule
var formule = {
    idformule : "",
    nom : "",
    reduction : "",
    details : "",
};
//création de la classe espace
var espace = {
    idespace : "",
    nom : "",
    detail : "",
    prix : "",
};

// création des types de chambre
var chambreDouble = Object.assign({}, espace);
var chambreFamille = Object.assign({}, espace);
var appartement = Object.assign({}, espace);

//cret=ation des formules
var weekendAmour = Object.assign({}, formule);
var nuitDecouverte = Object.assign({}, formule);
var travailSaisonnier = Object.assign({}, formule);

// affectation des valeurs pour les chambres double
chambreDouble.idespace = 1;
chambreDouble.nom = "chambre double";
chambreDouble.detail = "chambre de folie, avec le jacouzi qui va bien, le lit qui déchire, tu va te mettre bien taré (lit double + douche + WC)";
chambreDouble.prix = 35;

// affectation des valeurs pour les chambres famille
chambreFamille.idespace = 2;
chambreFamille.nom = "chambre familliale";
chambreFamille.detail = "chambre pour toute la famille, la femme, les enfants, les tantes, les mamis ... etc (lit double + 1 lit superposé + 1 lit simple + douche + WC)";
chambreFamille.prix = 55;

// affectation des valeurs pour les appartement
appartement.idespace = 3;
appartement.nom = "Appartement";
appartement.detail = "Appartement de grand malade, avec la sono, le hamam, free bar... tous ce dont tu a besoin pour passer des bête de vacance (prostitué non fournie) (2 lits doubles + 2 lits superposés + 2 lits simples + cuisine + douche + WC)";
appartement.prix = 110;

//affectation des valeur pour la formule weekendAmour
weekendAmour.idformule = 1;
weekendAmour.nom = "weekend d’amour";
weekendAmour.reduction = 10;
weekendAmour.details = "La formule « weekend d’amour » applique une réduction de 10% sur le nombre total de nuitées avec petit déjeuner « lover » servi au lit (uniquement pour les chambres pour deux personnes).";

//affectation des valeur pour la formule nuitDecouverte
nuitDecouverte.idformule = 2;
nuitDecouverte.nom = "nature et découverte";
nuitDecouverte.reduction = 5;
nuitDecouverte.details = "La formule « nature et découverte » est proposée pour tous les type d’espaces lorsque le nombre de nuitées et supérieure à 5 jours (weekend compris) elle applique une réduction de 5% sur la totalité du séjour. Elle propose en outre, une escapade « rahan » en terre du Gévaudan (le coutelas est fourni)";

//affectation des valeur pour la formule travailSaisonnier
travailSaisonnier.idformule = 3;
travailSaisonnier.nom = "travail saisonnier";
travailSaisonnier.reduction = 20;
travailSaisonnier.details = "La formule « travail saisonnier » applique une réduction de 20% sur la totalité du séjour (non majoré) pour tous les types d’espaces avec une réservation de 1 mois minimum pour les périodes de juin à aout (inclus) et de décembre à mars (inclus).";

//manipulation du dom pour changer les descriptions, nom... (en cour de compréhension)
$(".chambreDouble_nom").text(chambreDouble.nom);
$(".chambreDouble_detail").text(chambreDouble.detail);
$(".chambreDouble_prix").text(chambreDouble.prix);

$(".chambreFamille_nom").text(chambreFamille.nom);
$(".chambreFamille_detail").text(chambreFamille.detail);
$(".chambreFamille_prix").text(chambreFamille.prix);

$(".appartement_nom").text(appartement.nom);
$(".appartement_detail").text(appartement.detail);
$(".appartement_prix").text(appartement.prix);

$(document).ready(function() {
   //on créer le tableau espace
   var espaces = [];
   //on va le remplir avec nos différent espace
    espaces.push(chambreDouble);
    espaces.push(chambreFamille);
    espaces.push(appartement);

    //maintenant on boucle
    for (espace of espaces) {
        var cloneEspace = $("#cibleEspace").clone();
        //$(cloneEspace).find("#idEspace").text(espace.id)
        $(cloneEspace).find(".chambreNom").text(espace.nom).removeAttr("class");
        $(cloneEspace).find(".chambrePrix").text(espace.prix).removeAttr("class");
        $(cloneEspace).find(".chambreDetail").text(espace.detail).removeAttr("class");
        $(cloneEspace).show();
        $("#class_espace").append(cloneEspace);
    }
});

